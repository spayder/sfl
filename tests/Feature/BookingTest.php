<?php
namespace Tests\Feature;

use Tests\TestCase;

class BookingTest extends TestCase
{
    /** @test*/
    public function it_call_stats_successfully(): void
    {
        $params[] = [
            'request_id' => 'bookata_XY123',
            'check_in' => '2020-01-01',
            'nights' => 5,
            'selling_rate' => 200,
            'margin' => 20
        ];

        $response = $this->post('/stats', $params);

        $response->assertStatus(200);
        $response->assertJson([
            'avg_night' => 8,
            'min_night' => 8,
            'max_night' => 8
        ]);
    }

    /** @test*/
    public function it_call_maximize_successfully(): void
    {
        $params= [[
            'request_id' => 'bookata_XY123',
            'check_in' => '2020-01-01',
            'nights' => 5,
            'selling_rate' => 200,
            'margin' => 20
        ],
            [
                'request_id' => 'kayete_PP234',
                'check_in' => '2020-01-04',
                'nights' => 4,
                'selling_rate' => 156,
                'margin' => 5
            ],
            [
                'request_id' => 'atropote_AA930',
                'check_in' => '2020-01-04',
                'nights' => 4,
                'selling_rate' => 150,
                'margin' => 6
            ],
            [
                'request_id' => 'acme_AAAAA',
                'check_in' => '2020-01-10',
                'nights' => 4,
                'selling_rate' => 160,
                'margin' => 30
            ]];

        $response = $this->post('/maximize', $params);

        $response->assertStatus(200);
        $response->assertJson([
            'request_ids' =>  [
                'bookata_XY123',
                'acme_AAAAA'
            ],
            'total_profit' => 88,
            'avg_night' => 10,
            'min_night' => 8,
            'max_night' => 12
        ]);
    }

    /** @test */
    public function it_throws_an_error_when_no_date_was_sent()
    {
        $params[] = [
            'request_id' => 'bookata_XY123',
            'nights' => 5,
            'selling_rate' => 200,
            'margin' => 20
        ];

        $response = $this->post('/stats', $params);
        $response->assertStatus(400);

        $content = json_decode($response->getContent());

        $this->assertFalse($content->meta->success);
        $this->assertEmpty($content->data);
        $this->assertNotEmpty($content->meta->errors);
    }
}
