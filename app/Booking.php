<?php

namespace App;

ini_set( 'serialize_precision', -1 );


use Illuminate\Support\Carbon;

class Booking
{
    public function calculateStats($bookings): array
    {
        $rates = [];
        foreach ($bookings as $booking) {
            $rates[] = $this->calculateRate($booking);
        }

        return [
            'avg_night' => round(array_sum($rates) / count($rates), 2),
            'min_night' => min($rates),
            'max_night' => max($rates)
        ];
    }

    public function getMaximize($bookings): array
    {
        $result = [];

        foreach ($bookings as $key => $booking) {
            $booking['rate'] = $this->calculateRate($booking);
            $booking['check_out'] = (new Carbon($booking['check_in']))->addDays($booking['nights'])->format('Y-m-d');
            $booking['profit'] = $booking['selling_rate'] * ($booking['margin'] / 100);

            $result = $this->challenge($booking, $result);
        }

        return [
            'request_ids' => array_column($result, 'request_id'),
            'total_profit' => round(array_sum(array_column($result, 'profit')), 2),
            'avg_night' => round(array_sum(array_column($result, 'rate')) / count($result), 2),
            'min_night' => round(min(array_column($result, 'rate')),2),
            'max_night' => round(max(array_column($result, 'rate')), 2)
        ];
    }

    private function challenge($booking, $result): array
    {
        $winner = false;

        if (empty($result)) {
            return array_merge($result, [$booking]);
        }

        foreach ($result as $key => $item) {
            if (! $this->isOverlap($booking, $item)) {
                $winner = true;
            } else {
                $winner = false;
            }

            if ($winner === false && $this->isHigherRank($booking, $item)) {
                array_pop($result);
                $winner = true;
            }
        }

        if ($winner) {
            return array_merge($result, [$booking]);
        }

        return $result;
    }

    /**
     * @param $booking
     * @return float|int
     */
    private function calculateRate($booking)
    {
        return ($booking['selling_rate'] * ($booking['margin'] / 100)) / $booking['nights'];
    }

    /**
     * @param $booking
     * @param $item
     * @return bool
     */
    private function isOverlap($booking, $item): bool
    {
        $start_date = strtotime($item['check_in']);
        $end_date = strtotime($item['check_out']);
        $check_in = strtotime($booking['check_in']);

        return (($check_in >= $start_date) && ($check_in < $end_date));
    }

    private function isHigherRank($booking, $item): bool
    {
        return $booking['rate'] > $item['rate'];
    }
}
