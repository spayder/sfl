<?php
namespace Higgs\Http\Controllers;

use App\Booking;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    public function post(Request $request): JsonResponse
    {
        $data = $request->validate([
            '*.request_id' => 'required|alpha_dash',
            '*.check_in' => 'required|date|date_format:Y-m-d|after:today',
            '*.nights' => 'required|int',
            '*.selling_rate' => 'required|int',
            '*.margin' => 'required|int'
        ]);

        return response()->json((new Booking())->calculateStats($data));
    }

    public function maximize(Request $request): JsonResponse
    {
        $data = $request->validate([
            '*.request_id' => 'required|alpha_dash',
            '*.check_in' => 'required|date|date_format:Y-m-d|after:today',
            '*.nights' => 'required|int',
            '*.selling_rate' => 'required|int',
            '*.margin' => 'required|int'
        ]);

        return response()->json((new Booking())->getMaximize($data));
    }
}
