# Demo Project for StayForLong

## About This project

This is a simple demo project made with Laravel Framework (PHP). Has two endpoints /stats
and /maximize.

## Installation

To get this working, you need to install dependencies and set up your .env...   
* run ```composer install```   
* run ```cp .env.example .env```
* run ```php artisan key:generate```

## Resources 

[Laravel](http://www.laravel.com) (obviously) for the framework
